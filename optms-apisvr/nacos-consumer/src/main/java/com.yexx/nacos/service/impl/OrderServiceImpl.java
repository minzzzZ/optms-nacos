package com.yexx.nacos.service.impl;

import com.yexx.nacos.entity.OrderDO;
import com.yexx.nacos.mapper.OrderMapper;
import com.yexx.nacos.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Description: 测试service
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/10/14-11:28
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public String insert(OrderDO orderDO) {
        int insert = orderMapper.insert(orderDO);
        return "success count: " + insert;
    }

    @Override
    public String update(OrderDO orderDO) {
        OrderDO orderdb = orderMapper.selectById(orderDO.getId());
        orderdb.setMoney(orderdb.getMoney() + 1);
        orderMapper.updateById(orderdb);
        return null;
    }
}
