package com.yexx.nacos.service;

import com.yexx.nacos.entity.OrderDO;

/**
 * Description: 测试service
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/10/14-11:29
 */
public interface OrderService {
    String insert(OrderDO orderDO);
    String update(OrderDO orderDO);
}
