//package com.yexx.nacos.config.handler;
//
//import com.yexx.core.entity.ApiResponse;
//import com.yexx.core.exception.CommonException;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import static com.yexx.starter.security.basic.constant.SecurityConstants.RETURN_CODE.SYS_ERROR;
//
//@Slf4j
//@ControllerAdvice
//public class CommonExceptionHandler {
//
//    @ResponseBody
//    @ExceptionHandler(Exception.class)
//    public ApiResponse exceptionHandler(Exception e) {
//        CommonException ce = null;
//        if (e instanceof CommonException) {
//            ce = (CommonException) e;
//            log.debug(e.getMessage());
//        } else {
//            ce = new CommonException(SYS_ERROR, e.getMessage());
//            log.debug(e.getMessage(), e);
//        }
//        return ApiResponse.fail(ce);
//    }
//
//}
