package com.yexx.nacos.config.security;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yexx.starter.security.basic.UserContext;
import com.yexx.core.serializer.JsonLongSerializer;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Description: 系统用户
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/11/03-09:50
 */
public class SysUserContext extends UserContext {

//    private static final long serialVersionUID = 3262709901856658079L;

    public SysUserContext(Long accountId, String phoneNumber, String password, Collection<? extends GrantedAuthority> authorities) {
        super(accountId, phoneNumber, password, authorities);
    }

    public SysUserContext() {}

    @JsonSerialize(using = JsonLongSerializer.class)
    private Long userId;
    private Integer pwdStatus;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getPwdStatus() {
        return pwdStatus;
    }

    public void setPwdStatus(Integer pwdStatus) {
        this.pwdStatus = pwdStatus;
    }
}
