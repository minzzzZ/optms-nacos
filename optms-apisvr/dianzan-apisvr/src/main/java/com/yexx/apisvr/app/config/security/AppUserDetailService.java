package com.yexx.apisvr.app.config.security;

import com.yexx.starter.security.basic.SecurityUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * Description: 登录service
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/11/25-15:00
 */
@Service
public class AppUserDetailService implements SecurityUserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) {
        return null;
    }
}
