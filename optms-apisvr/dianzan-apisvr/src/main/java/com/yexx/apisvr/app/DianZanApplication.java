package com.yexx.apisvr.app;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.util.StopWatch;

/**
 * Description: 启动类
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/11/25-14:14
 */
@Slf4j
@ConfigurationPropertiesScan("com.yexx")
@SpringBootApplication(scanBasePackages = "com.yexx")
public class DianZanApplication {

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("run-application");
        SpringApplication.run(DianZanApplication.class, args);
        stopWatch.stop();
        log.info("run end！ {}", stopWatch.prettyPrint());
    }
}
