package com.yexx.apisvr.app;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Description: 测试
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/11/25-15:06
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DianZanApplication.class)
public class DianZanApplicationText {

    @Test
    public void ContextLoad() {
        log.info("load context success!!!");
    }
}
