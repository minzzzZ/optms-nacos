<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.0.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <groupId>com.yexx</groupId>
    <artifactId>optms-cloud</artifactId>
    <version>1.0.0</version>
    <packaging>pom</packaging>

    <modules>
        <!--微服务层-->
        <module>optms-microsvr</module>
        <!--启动项-->
        <module>optms-starter</module>
        <!--common-->
        <module>optms-common</module>
        <!--搜索服务-->
        <module>optms-search</module>
        <!--服务监控-->
        <module>optms-monitor</module>
        <!--对外api-->
        <module>optms-apisvr</module>
        <!--代码生成器-->
        <module>optms-generator</module>
        <!--网关-->
        <module>optms-gateway</module>
    </modules>

    <properties>
        <!--springcloud Hoxton.SR3 对应springcloud-alibaba 2.2.1.RELEASE -->
        <!--springcloud Hoxton.SR1 对应springcloud-alibaba 2.2.0.RELEASE -->
        <spring.boot.version>2.3.0.RELEASE</spring.boot.version>
        <spring.cloud.version>Hoxton.SR3</spring.cloud.version>
        <spring.cloud.alibaba.version>2.2.1.RELEASE</spring.cloud.alibaba.version>
        <optms.core.version>2.0.0</optms.core.version> <!--用于版本升级-->
        <druid.version>1.1.22</druid.version>
        <mybatis.version>1.3.2</mybatis.version>
        <mybatis-plus-boot-starter.version>3.3.2</mybatis-plus-boot-starter.version>
        <admin-actuator.version>2.3.1</admin-actuator.version>
        <knife4j.version>2.0.5</knife4j.version>
        <seata.spring-boot.version>1.3.0</seata.spring-boot.version>
        <springfox.version>2.9.2</springfox.version>
        <org.mapstruct.version>1.4.1.Final</org.mapstruct.version>
        <alibaba.transmittable.version>2.11.5</alibaba.transmittable.version>
        <userAgent.version>1.21</userAgent.version>
    </properties>

    <!--声明依赖 版本定义-->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring.cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring.cloud.alibaba.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!--admin actuator-->
            <dependency>
                <groupId>de.codecentric</groupId>
                <artifactId>spring-boot-admin-starter-client</artifactId>
                <version>${admin-actuator.version}</version>
            </dependency>
            <dependency>
                <groupId>de.codecentric</groupId>
                <artifactId>spring-boot-admin-starter-server</artifactId>
                <version>${admin-actuator.version}</version>
            </dependency>
            <dependency>
                <groupId>de.codecentric</groupId>
                <artifactId>spring-boot-admin-server-ui</artifactId>
                <version>${admin-actuator.version}</version>
            </dependency>

            <!--mybatis plus-->
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-boot-starter</artifactId>
                <version>${mybatis-plus-boot-starter.version}</version>
            </dependency>
            <!--springfox for swagger-->
            <dependency>
                <groupId>io.springfox</groupId>
                <artifactId>springfox-swagger2</artifactId>
                <version>${springfox.version}</version>
            </dependency>

            <dependency>
                <groupId>com.github.xiaoymin</groupId>
                <artifactId>knife4j-spring-boot-starter</artifactId>
                <version>${knife4j.version}</version>
            </dependency>

            <!--common-->
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>optms-core</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>optms-uid</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>io.seata</groupId>
                <artifactId>seata-spring-boot-starter</artifactId>
                <version>${seata.spring-boot.version}</version>
            </dependency>
            <dependency>
                <groupId>eu.bitwalker</groupId>
                <artifactId>UserAgentUtils</artifactId>
                <version>${userAgent.version}</version>
            </dependency>

            <!--自定义starter-->
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>spring-boot-optms-cache-starter</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>spring-boot-optms-core-starter</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>spring-boot-optms-jdbc-starter</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>spring-boot-optms-logger-starter</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>spring-boot-optms-security-starter</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>spring-boot-optms-session-starter</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>spring-boot-optms-swagger-starter</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>spring-boot-optms-ribbon-starter</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>spring-boot-optms-ribbon-starter</artifactId>
                <version>${optms.core.version}</version>
            </dependency>

            <!--tools-->
            <!--MDC增强-->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>transmittable-thread-local</artifactId>
                <version>${alibaba.transmittable.version}</version>
            </dependency>

            <!--微服务-->
            <dependency>
                <groupId>com.yexx.service.api</groupId>
                <artifactId>user-api</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx.service.entity</groupId>
                <artifactId>user-entity</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx</groupId>
                <artifactId>nacos-provider-api</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx.service.api</groupId>
                <artifactId>simple-api</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.yexx.service.entity</groupId>
                <artifactId>simple-entity</artifactId>
                <version>${optms.core.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
        <dependency>
            <groupId>org.mapstruct</groupId>
            <artifactId>mapstruct</artifactId>
            <version>${org.mapstruct.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mapstruct</groupId>
            <artifactId>mapstruct-processor</artifactId>
            <version>${org.mapstruct.version}</version>
        </dependency>
    </dependencies>

    <build>
        <finalName>${project.artifactId}</finalName>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>11</source>
                    <target>11</target>
                    <encoding>${project.build.sourceEncoding}</encoding>
                </configuration>
            </plugin>
            <!--mvn install -Dmaven.test.skip=true-->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>public</id>
            <name>aliyun nexus</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>public</id>
            <name>aliyun nexus</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>Min Zuo</name>
            <email>myleszelic@outlook.com</email>
            <organization>optms-team</organization>
        </developer>
        <developer>
            <name>fzkjjt</name>
            <email>1530726809@qq.com</email>
            <organization>optms-team</organization>
        </developer>
        <developer>
            <name>Dandelion</name>
            <email>duqianhe@worktrans.cn</email>
            <organization>optms-team</organization>
        </developer>
        <developer>
            <name>DoubleJee</name>
            <email>gongzhenzhen@worktrans.cn</email>
            <organization>optms-team</organization>
        </developer>
    </developers>


</project>