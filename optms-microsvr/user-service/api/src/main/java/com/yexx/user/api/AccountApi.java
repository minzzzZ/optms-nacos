package com.yexx.user.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yexx.core.entity.ApiResponse;
import com.yexx.user.domain.Account;
import com.yexx.user.request.account.*;
import com.yexx.core.constant.FeignConstants;
import com.yexx.user.fallback.AccountFallbackFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 账户 Api
 * 
 * @author code Generator
 * @date 2020-12-02
 */
@Api(value = "账户", tags = {"账户"})
@FeignClient(value = FeignConstants.USER_SERVICE, fallbackFactory = AccountFallbackFactory.class, contextId = "account-api")
public interface AccountApi {

    @ApiOperation(value = "新增", response = Account.class)
    @PostMapping("/account/create")
    ApiResponse<Account> create(@RequestBody AccountCreateRequest request);

    @ApiOperation(value = "删除", response = Boolean.class)
    @PostMapping("/account/delete")
    ApiResponse delete(@RequestParam("id") Long id);

    @ApiOperation(value = "修改", response = Boolean.class)
    @PostMapping("/account/update")
    ApiResponse update(@RequestBody AccountUpdateRequest request);

    @ApiOperation(value = "通过主键查找", response = Account.class)
    @PostMapping("/account/findById")
    ApiResponse<Account> findById(@RequestParam("id") Long id);

    @ApiOperation(value = "根据主键查找", response = Boolean.class)
    @PostMapping("/account/findByIds")
    ApiResponse<Map<Long, Account>> findByIds(@RequestBody Set<Long> ids);

    @ApiOperation(value = "分页", response = Account.class)
    @PostMapping("/account/page")
    ApiResponse<IPage<Account>> page(@RequestBody AccountPageRequest request);

    @ApiOperation(value = "查询一个", response = Account.class)
    @PostMapping("/account/findOne")
    ApiResponse<Account> findOne(@RequestBody AccountSimpleRequest request);

    @ApiOperation(value = "查询一批", response = Account.class)
    @PostMapping("/account/limitList")
    ApiResponse<List<Account>> limitList(@RequestBody AccountLimitRequest request);
}
