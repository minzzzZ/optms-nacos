package com.yexx.user.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yexx.core.entity.ApiResponse;
import com.yexx.user.domain.User;
import com.yexx.user.request.user.*;
import com.yexx.core.constant.FeignConstants;
import com.yexx.user.fallback.UserFallbackFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 用户 Api
 * 
 * @author code Generator
 * @date 2020-12-02
 */
@Api(value = "用户", tags = {"用户"})
@FeignClient(value = FeignConstants.USER_SERVICE, fallbackFactory = UserFallbackFactory.class, contextId = "user-api")
public interface UserApi {

    @ApiOperation(value = "新增", response = User.class)
    @PostMapping("/user/create")
    ApiResponse<User> create(@RequestBody UserCreateRequest request);

    @ApiOperation(value = "删除", response = Boolean.class)
    @PostMapping("/user/delete")
    ApiResponse delete(@RequestParam("id") Long id);

    @ApiOperation(value = "修改", response = Boolean.class)
    @PostMapping("/user/update")
    ApiResponse update(@RequestBody UserUpdateRequest request);

    @ApiOperation(value = "通过主键查找", response = User.class)
    @PostMapping("/user/findById")
    ApiResponse<User> findById(@RequestParam("id") Long id);

    @ApiOperation(value = "根据主键查找", response = Boolean.class)
    @PostMapping("/user/findByIds")
    ApiResponse<Map<Long, User>> findByIds(@RequestBody Set<Long> ids);

    @ApiOperation(value = "分页", response = User.class)
    @PostMapping("/user/page")
    ApiResponse<IPage<User>> page(@RequestBody UserPageRequest request);

    @ApiOperation(value = "查询一个", response = User.class)
    @PostMapping("/user/findOne")
    ApiResponse<User> findOne(@RequestBody UserSimpleRequest request);

    @ApiOperation(value = "查询一批", response = User.class)
    @PostMapping("/user/limitList")
    ApiResponse<List<User>> limitList(@RequestBody UserLimitRequest request);
}
