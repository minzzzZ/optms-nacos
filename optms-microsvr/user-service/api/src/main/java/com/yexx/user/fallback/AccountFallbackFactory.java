package com.yexx.user.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 账户 fallbackFactory
 *
 * @author code Generator
 * @date 2020-12-02
 */
@Slf4j
@Component
public class AccountFallbackFactory<T> implements FallbackFactory {

    @Override
    public T create(Throwable throwable) {
        log.error("{}", "进入到容错信息的error打印");
        log.error("{}", throwable);
        throw new RuntimeException(throwable.getMessage());
    }
}
