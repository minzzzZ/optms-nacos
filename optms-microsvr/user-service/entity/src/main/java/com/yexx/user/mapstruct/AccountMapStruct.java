package com.yexx.user.mapstruct;

import com.yexx.user.domain.Account;
import com.yexx.user.request.account.AccountCreateRequest;
import com.yexx.user.request.account.AccountLimitRequest;
import com.yexx.user.request.account.AccountSimpleRequest;
import com.yexx.user.request.account.AccountUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * dto 转换
 *
 * @author code Generator
 * @date 2020-12-02
 */
@Mapper(componentModel = "spring")
//@Mapper
public interface AccountMapStruct {
    AccountMapStruct INSTANCE = Mappers.getMapper(AccountMapStruct.class);

    Account tranfer(AccountCreateRequest accountCreateRequest);

    Account tranfer(AccountLimitRequest accountLimitRequest);

    Account tranfer(AccountSimpleRequest accountSimpleRequest);

    Account tranfer(AccountUpdateRequest accountUpdateRequest);

}