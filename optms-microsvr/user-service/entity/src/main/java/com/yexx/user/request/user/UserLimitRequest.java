package com.yexx.user.request.user;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.yexx.core.entity.LimitRequest;
import com.yexx.user.domain.User;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 用户 分页参数 user
 *
 * @author code Generator
 * @date 2020-12-02
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "UserLimitRequest", description = "用户 分页参数")
public class UserLimitRequest extends LimitRequest {

    /** 账户id */
    @ApiModelProperty(value = "账户id")
    private Long accountId;

    /** 昵称 */
    @ApiModelProperty(value = "昵称")
    private String nickname;

    /** 手机号 */
    @ApiModelProperty(value = "手机号")
    private String phoneNumber;

    /** 密码 */
    @ApiModelProperty(value = "密码")
    private String pwd;

    /** 注册时间 */
    @ApiModelProperty(value = "注册时间")
    private LocalDateTime registerTime;

    /** 用户头像 */
    @ApiModelProperty(value = "用户头像")
    private String avatarUrl;

    /** 生日 */
    @ApiModelProperty(value = "生日")
    private LocalDate birth;

    /** 性别(0, 女 1, 男 2,未知) */
    @ApiModelProperty(value = "性别(0, 女 1, 男 2,未知)")
    private Integer gender;

    /** 用户简介 */
    @ApiModelProperty(value = "用户简介")
    private String intro;

    /** 用户状态(0, 正常 1,暂时封禁 2,永久封禁 3,全平台封禁) */
    @ApiModelProperty(value = "用户状态(0, 正常 1,暂时封禁 2,永久封禁 3,全平台封禁)")
    private Integer status;

    /** 用户登录状态 */
    @ApiModelProperty(value = "用户登录状态")
    private String loginStatus;

    /** 最近登录时间 */
    @ApiModelProperty(value = "最近登录时间")
    private LocalDateTime loginTime;

    /** 最近登录ip地址 */
    @ApiModelProperty(value = "最近登录ip地址")
    private String loginIp;

    /** 连签次数 */
    @ApiModelProperty(value = "连签次数")
    private Integer loginTimes;

    /** token */
    @ApiModelProperty(value = "token")
    private String accessToken;

    /** 邀请人id */
    @ApiModelProperty(value = "邀请人id")
    private Long inviteUserId;

    /** 所属城市 */
    @ApiModelProperty(value = "所属城市")
    private String city;


    @Override
    public QueryWrapper<User> toParamsQueryWapper() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        if (accountId != null) {
            queryWrapper.eq("account_id", accountId);
        }
        if (nickname != null) {
            queryWrapper.eq("nickname", nickname);
        }
        if (phoneNumber != null) {
            queryWrapper.eq("phone_number", phoneNumber);
        }
        if (pwd != null) {
            queryWrapper.eq("pwd", pwd);
        }
        if (registerTime != null) {
            queryWrapper.eq("register_time", registerTime);
        }
        if (avatarUrl != null) {
            queryWrapper.eq("avatar_url", avatarUrl);
        }
        if (birth != null) {
            queryWrapper.eq("birth", birth);
        }
        if (gender != null) {
            queryWrapper.eq("gender", gender);
        }
        if (intro != null) {
            queryWrapper.eq("intro", intro);
        }
        if (status != null) {
            queryWrapper.eq("status", status);
        }
        if (loginStatus != null) {
            queryWrapper.eq("login_status", loginStatus);
        }
        if (loginTime != null) {
            queryWrapper.eq("login_time", loginTime);
        }
        if (loginIp != null) {
            queryWrapper.eq("login_ip", loginIp);
        }
        if (loginTimes != null) {
            queryWrapper.eq("login_times", loginTimes);
        }
        if (accessToken != null) {
            queryWrapper.eq("access_token", accessToken);
        }
        if (inviteUserId != null) {
            queryWrapper.eq("invite_user_id", inviteUserId);
        }
        if (city != null) {
            queryWrapper.eq("city", city);
        }
        return queryWrapper;
    }

}
