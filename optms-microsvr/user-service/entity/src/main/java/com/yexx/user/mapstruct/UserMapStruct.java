package com.yexx.user.mapstruct;

import com.yexx.user.domain.User;
import com.yexx.user.request.user.UserCreateRequest;
import com.yexx.user.request.user.UserLimitRequest;
import com.yexx.user.request.user.UserSimpleRequest;
import com.yexx.user.request.user.UserUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * dto 转换
 *
 * @author code Generator
 * @date 2020-12-02
 */
@Mapper(componentModel = "spring")
//@Mapper
public interface UserMapStruct {
    UserMapStruct INSTANCE = Mappers.getMapper(UserMapStruct.class);

    User tranfer(UserCreateRequest userCreateRequest);

    User tranfer(UserLimitRequest userLimitRequest);

    User tranfer(UserSimpleRequest userSimpleRequest);

    User tranfer(UserUpdateRequest userUpdateRequest);

}