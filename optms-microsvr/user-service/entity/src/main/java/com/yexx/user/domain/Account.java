package com.yexx.user.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yexx.core.serializer.JsonLongSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 账户表 account
 * 
 * @author code Generator
 * @date 2020-12-02
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Account对象", description = "账户")
@TableName("account")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class Account extends Model<Account> implements Serializable {

	/** 主键 */
    @JsonSerialize(using = JsonLongSerializer.class)
    @TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "主键")
	private Long id;

	/** 账户昵称 */
	@ApiModelProperty(value = "账户昵称")
	private String name;

	/** 登录用户名 */
	@ApiModelProperty(value = "登录用户名")
	private String username;

	/** 密码-明文 */
	@ApiModelProperty(value = "密码-明文")
	private String pwd;

	/** 密码-密文 */
	@ApiModelProperty(value = "密码-密文")
	private String secret;

	/** '密码状态 1:正常 2:空 3:系统初始化 4:需重置 5:简单' */
	@ApiModelProperty(value = "'密码状态 1:正常 2:空 3:系统初始化 4:需重置 5:简单'")
	private Integer pwdStatus;

	/** 手机号 */
	@ApiModelProperty(value = "手机号")
	private String phoneNumber;

	/** 邮箱 */
	@ApiModelProperty(value = "邮箱")
	private String email;

	/** 头像 */
	@ApiModelProperty(value = "头像")
	private String avatarUrl;

	/** 性别(0, 女 1, 男 2,未知) */
	@ApiModelProperty(value = "性别(0, 女 1, 男 2,未知)")
	private Integer gender;

	/** 身份证号 */
	@ApiModelProperty(value = "身份证号")
	private String idCardNo;

	/** 禁用状态(1, 禁用 0,启用) */
	@ApiModelProperty(value = "禁用状态(1, 禁用 0,启用)")
	private Boolean disable;

	/** 第一次注册时间 */
	@ApiModelProperty(value = "第一次注册时间")
	private LocalDateTime registerTime;

	/** 创建时间 */
	@ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
	private LocalDateTime createTime;

	/** 更新时间 */
	@ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
	private LocalDateTime updateTime;


}
