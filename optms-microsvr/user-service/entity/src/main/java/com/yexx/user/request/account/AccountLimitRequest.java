package com.yexx.user.request.account;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.yexx.core.entity.LimitRequest;
import com.yexx.user.domain.Account;

import java.time.LocalDateTime;

/**
 * 账户 分页参数 account
 *
 * @author code Generator
 * @date 2020-12-02
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "AccountLimitRequest", description = "账户 分页参数")
public class AccountLimitRequest extends LimitRequest {

    /** 账户昵称 */
    @ApiModelProperty(value = "账户昵称")
    private String name;

    /** 登录用户名 */
    @ApiModelProperty(value = "登录用户名")
    private String username;

    /** 密码-明文 */
    @ApiModelProperty(value = "密码-明文")
    private String pwd;

    /** 密码-密文 */
    @ApiModelProperty(value = "密码-密文")
    private String secret;

    /** '密码状态 1:正常 2:空 3:系统初始化 4:需重置 5:简单' */
    @ApiModelProperty(value = "'密码状态 1:正常 2:空 3:系统初始化 4:需重置 5:简单'")
    private Integer pwdStatus;

    /** 手机号 */
    @ApiModelProperty(value = "手机号")
    private String phoneNumber;

    /** 邮箱 */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /** 头像 */
    @ApiModelProperty(value = "头像")
    private String avatarUrl;

    /** 性别(0, 女 1, 男 2,未知) */
    @ApiModelProperty(value = "性别(0, 女 1, 男 2,未知)")
    private Integer gender;

    /** 身份证号 */
    @ApiModelProperty(value = "身份证号")
    private String idCardNo;

    /** 禁用状态(1, 禁用 0,启用) */
    @ApiModelProperty(value = "禁用状态(1, 禁用 0,启用)")
    private Boolean disable;

    /** 第一次注册时间 */
    @ApiModelProperty(value = "第一次注册时间")
    private LocalDateTime registerTime;


    @Override
    public QueryWrapper<Account> toParamsQueryWapper() {
        QueryWrapper<Account> queryWrapper = new QueryWrapper();
        if (name != null) {
            queryWrapper.eq("name", name);
        }
        if (username != null) {
            queryWrapper.eq("username", username);
        }
        if (pwd != null) {
            queryWrapper.eq("pwd", pwd);
        }
        if (secret != null) {
            queryWrapper.eq("secret", secret);
        }
        if (pwdStatus != null) {
            queryWrapper.eq("pwd_status", pwdStatus);
        }
        if (phoneNumber != null) {
            queryWrapper.eq("phone_number", phoneNumber);
        }
        if (email != null) {
            queryWrapper.eq("email", email);
        }
        if (avatarUrl != null) {
            queryWrapper.eq("avatar_url", avatarUrl);
        }
        if (gender != null) {
            queryWrapper.eq("gender", gender);
        }
        if (idCardNo != null) {
            queryWrapper.eq("id_card_no", idCardNo);
        }
        if (disable != null) {
            queryWrapper.eq("disable", disable);
        }
        if (registerTime != null) {
            queryWrapper.eq("register_time", registerTime);
        }
        return queryWrapper;
    }

}
