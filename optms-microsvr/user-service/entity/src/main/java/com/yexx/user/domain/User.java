package com.yexx.user.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import lombok.*;
import lombok.experimental.Accessors;

import com.yexx.core.serializer.JsonLongSerializer;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 用户表 user
 * 
 * @author code Generator
 * @date 2020-12-02
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "User对象", description = "用户")
@TableName("user")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class User extends Model<User> implements Serializable {

	/** 主键 */
    @JsonSerialize(using = JsonLongSerializer.class)
    @TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "主键")
	private Long id;

	/** 账户id */
    @JsonSerialize(using = JsonLongSerializer.class)
	@ApiModelProperty(value = "账户id")
	private Long accountId;

	/** 昵称 */
	@ApiModelProperty(value = "昵称")
	private String nickname;

	/** 手机号 */
	@ApiModelProperty(value = "手机号")
	private String phoneNumber;

	/** 密码 */
	@ApiModelProperty(value = "密码")
	private String pwd;

	/** 注册时间 */
	@ApiModelProperty(value = "注册时间")
	private LocalDateTime registerTime;

	/** 用户头像 */
	@ApiModelProperty(value = "用户头像")
	private String avatarUrl;

	/** 生日 */
	@ApiModelProperty(value = "生日")
	private LocalDate birth;

	/** 性别(0, 女 1, 男 2,未知) */
	@ApiModelProperty(value = "性别(0, 女 1, 男 2,未知)")
	private Integer gender;

	/** 用户简介 */
	@ApiModelProperty(value = "用户简介")
	private String intro;

	/** 用户状态(0, 正常 1,暂时封禁 2,永久封禁 3,全平台封禁) */
	@ApiModelProperty(value = "用户状态(0, 正常 1,暂时封禁 2,永久封禁 3,全平台封禁)")
	private Integer status;

	/** 用户登录状态 */
	@ApiModelProperty(value = "用户登录状态")
	private String loginStatus;

	/** 最近登录时间 */
	@ApiModelProperty(value = "最近登录时间")
	private LocalDateTime loginTime;

	/** 最近登录ip地址 */
	@ApiModelProperty(value = "最近登录ip地址")
	private String loginIp;

	/** 连签次数 */
	@ApiModelProperty(value = "连签次数")
	private Integer loginTimes;

	/** token */
	@ApiModelProperty(value = "token")
	private String accessToken;

	/** 邀请人id */
    @JsonSerialize(using = JsonLongSerializer.class)
	@ApiModelProperty(value = "邀请人id")
	private Long inviteUserId;

	/** 所属城市 */
	@ApiModelProperty(value = "所属城市")
	private String city;


}
