package com.yexx.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yexx.core.entity.ApiResponse;
import com.yexx.core.exception.CommonException;
import com.yexx.uid.IdGenerator;
import com.yexx.user.api.UserApi;
import com.yexx.user.domain.User;
import com.yexx.user.mapstruct.UserMapStruct;
import com.yexx.user.request.user.*;
import com.yexx.user.service.UserService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.exceptions.TooManyResultsException;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 用户 controller
 *
 * @author code Generator
 * @date 2020-12-02
 */
@Slf4j
@Api(value = "用户", tags = {"用户"})
@RestController
public class UserController implements UserApi {

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapStruct userMapStruct;

    @Override
    public ApiResponse<User> create(UserCreateRequest request) {
        User user = userMapStruct.tranfer(request);
        userService.save(user);
        return ApiResponse.success(user);
    }

    @Override
    public ApiResponse delete(Long id) {
        userService.removeById(id);
        return ApiResponse.success();
    }

    @Override
    public ApiResponse update(UserUpdateRequest request) {
        User user = userMapStruct.tranfer(request);
        userService.updateById(user);
        return ApiResponse.success();
    }

    @Override
    public ApiResponse<User> findById(Long id) {
        return ApiResponse.success(userService.getById(id));
    }

    @Override
    public ApiResponse<Map<Long, User>> findByIds(Set<Long> ids) {
        Map<Long, User> map = new HashMap(10);
        List<User> list = userService.listByIds(ids);
        list.forEach((item) -> map.put(item.getId(), item));
        return ApiResponse.success(map);
    }

    @Override
    public ApiResponse<IPage<User>> page(UserPageRequest request) {
        IPage<User> page = userService.page(request.getPage(), request.toQueryWapper());
        return ApiResponse.success(page);
    }

    @Override
    public ApiResponse<User> findOne(UserSimpleRequest request) {
        User user = null;
        try {
            user = userService.getOne(request.toParamsQueryWapper());
        } catch (MyBatisSystemException e) {
            if (e.getCause() instanceof TooManyResultsException) {
                throw new CommonException(31001, "查询结果不唯一");
            } else {
                e.printStackTrace();
            }
        }
        return ApiResponse.success(user);
    }

    @Override
    public ApiResponse<List<User>> limitList(UserLimitRequest request) {
        List<User> list = userService.list(request.toQueryWapper());
        return ApiResponse.success(list);
    }

}
