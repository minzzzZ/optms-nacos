package com.yexx.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yexx.core.entity.ApiResponse;
import com.yexx.core.exception.CommonException;
import com.yexx.uid.IdGenerator;
import com.yexx.user.api.AccountApi;
import com.yexx.user.domain.Account;
import com.yexx.user.mapstruct.AccountMapStruct;
import com.yexx.user.request.account.*;
import com.yexx.user.service.AccountService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.exceptions.TooManyResultsException;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 账户 controller
 *
 * @author code Generator
 * @date 2020-12-02
 */
@Slf4j
@Api(value = "账户", tags = {"账户"})
@RestController
public class AccountController implements AccountApi {

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private AccountMapStruct accountMapStruct;

    @Override
    public ApiResponse<Account> create(AccountCreateRequest request) {
        Account account = accountMapStruct.tranfer(request);
        accountService.save(account);
        return ApiResponse.success(account);
    }

    @Autowired
    private AccountService accountService;

    @Override
    public ApiResponse delete(Long id) {
        accountService.removeById(id);
        return ApiResponse.success();
    }

    @Override
    public ApiResponse update(AccountUpdateRequest request) {
        Account account = accountMapStruct.tranfer(request);
        accountService.updateById(account);
        return ApiResponse.success();
    }

    @Override
    public ApiResponse<Account> findById(Long id) {
        return ApiResponse.success(accountService.getById(id));
    }

    @Override
    public ApiResponse<Map<Long, Account>> findByIds(Set<Long> ids) {
        Map<Long, Account> map = new HashMap(10);
        List<Account> list = accountService.listByIds(ids);
        list.forEach((item) -> map.put(item.getId(), item));
        return ApiResponse.success(map);
    }

    @Override
    public ApiResponse<IPage<Account>> page(AccountPageRequest request) {
        IPage<Account> page = accountService.page(request.getPage(), request.toQueryWapper());
        return ApiResponse.success(page);
    }

    @Override
    public ApiResponse<Account> findOne(AccountSimpleRequest request) {
        Account account = null;
        try {
            account = accountService.getOne(request.toParamsQueryWapper());
        } catch (MyBatisSystemException e) {
            if (e.getCause() instanceof TooManyResultsException) {
                throw new CommonException(31001, "查询结果不唯一");
            } else {
                e.printStackTrace();
            }
        }
        return ApiResponse.success(account);
    }

    @Override
    public ApiResponse<List<Account>> limitList(AccountLimitRequest request) {
        List<Account> list = accountService.list(request.toQueryWapper());
        return ApiResponse.success(list);
    }

}
