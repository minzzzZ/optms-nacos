package com.yexx.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.yexx.user.mapper.AccountMapper;
import com.yexx.user.domain.Account;
import com.yexx.user.service.AccountService;

/**
 * 账户 服务层实现
 * 
 * @author code Generator
 * @date 2020-12-02
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

    @Autowired
    private AccountMapper userMapper;

}
