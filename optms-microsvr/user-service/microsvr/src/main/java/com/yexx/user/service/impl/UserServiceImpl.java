package com.yexx.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.yexx.user.mapper.UserMapper;
import com.yexx.user.domain.User;
import com.yexx.user.service.UserService;

/**
 * 用户 服务层实现
 * 
 * @author code Generator
 * @date 2020-12-02
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

}
