package com.yexx.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yexx.user.domain.User;

/**
 * 用户 服务层
 * 
 * @author code Generator
 * @date 2020-12-02
 */
public interface UserService extends IService<User> {

}
