package com.yexx.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.util.StopWatch;

/**
 * Description: 启动配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/08/11-15:13
 */
@Slf4j
@ConfigurationPropertiesScan("com.yexx")
@SpringBootApplication(scanBasePackages = "com.yexx")
public class UserApplication {

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("run-application");
        SpringApplication.run(UserApplication.class, args);
        stopWatch.stop();
        log.info("run end！ {}", stopWatch.prettyPrint());
    }

}