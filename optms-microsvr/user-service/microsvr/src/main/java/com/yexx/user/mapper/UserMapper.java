package com.yexx.user.mapper;

import com.yexx.cache.CacheHelper;
import com.yexx.mybatis.CommonMapper;
import com.yexx.user.domain.User;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户 数据层
 * 
 * @author code Generator
 * @date 2020-12-02
 */
@Mapper
@CacheNamespace(implementation = CacheHelper.class, eviction = CacheHelper.class)
public interface UserMapper extends CommonMapper<User> {

}