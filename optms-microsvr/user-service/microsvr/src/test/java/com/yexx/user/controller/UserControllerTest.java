package com.yexx.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yexx.core.entity.ApiResponse;
import com.yexx.user.UserApplication;
import com.yexx.user.request.user.*;
import com.yexx.user.domain.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * 用户 controller 测试
 *
 * @author code Generator
 * @date 2020-12-02
 */
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserApplication.class)
public class UserControllerTest {

    @Autowired
    public UserController controller;

    @Test
    public void CRUDTest() {

        UserCreateRequest createRequest = UserCreateRequest.builder()
            .accountId(111L)
            .nickname("TEST")
            .phoneNumber("TEST")
            .pwd("TEST")
            .avatarUrl("TEST")
            .gender(111)
            .intro("TEST")
            .status(111)
            .loginStatus("TEST")
            .loginIp("TEST")
            .loginTimes(111)
            .accessToken("TEST")
            .inviteUserId(111L)
            .city("TEST")
            .build();

        //insert
        ApiResponse<User> createResp = controller.create(createRequest);
        Assert.assertTrue(createResp.getCode() == 10000);

        //update
        Long id = createResp.getData().getId();
        UserUpdateRequest updateRequest = UserUpdateRequest.builder().id(id)
                .nickname("test")
                .phoneNumber("test")
                .pwd("test")
                .avatarUrl("test")
                .intro("test")
                .loginStatus("test")
                .loginIp("test")
                .accessToken("test")
                .city("test")
                .build();
        ApiResponse updateResp = controller.update(updateRequest);
        Assert.assertTrue(updateResp.getCode() == 10000);

        //find by id
        ApiResponse<User> findByIdResp = controller.findById(id);
        User dox = findByIdResp.getData();

        Assert.assertTrue("test".equals(dox.getNickname()));
        Assert.assertTrue("test".equals(dox.getPhoneNumber()));
        Assert.assertTrue("test".equals(dox.getPwd()));
        Assert.assertTrue("test".equals(dox.getAvatarUrl()));
        Assert.assertTrue("test".equals(dox.getIntro()));
        Assert.assertTrue("test".equals(dox.getLoginStatus()));
        Assert.assertTrue("test".equals(dox.getLoginIp()));
        Assert.assertTrue("test".equals(dox.getAccessToken()));
        Assert.assertTrue("test".equals(dox.getCity()));

        //fond map
        Set<Long> ids = new HashSet<>();
        ids.add(id);
        ApiResponse<Map<Long, User>> findByIdsResp = controller.findByIds(ids);
        Map map = findByIdsResp.getData();
        Assert.assertTrue(map.containsKey(id));

        //page
        UserPageRequest pageRequest = UserPageRequest.builder()
                .nickname("test")
                .phoneNumber("test")
                .pwd("test")
                .avatarUrl("test")
                .intro("test")
                .loginStatus("test")
                .loginIp("test")
                .accessToken("test")
                .city("test")
                .build();
        pageRequest.setPageNum(1);
        pageRequest.setPageSize(10);
        ApiResponse<IPage<User>> pageResp = controller.page(pageRequest);
        Assert.assertTrue(pageResp.getCode() == 10000);


        UserLimitRequest limitRequest = UserLimitRequest.builder()
                .nickname("test")
                .phoneNumber("test")
                .pwd("test")
                .avatarUrl("test")
                .intro("test")
                .loginStatus("test")
                .loginIp("test")
                .accessToken("test")
                .city("test")
                .build();
        limitRequest.setOffset(0);
        limitRequest.setLimit(10);


        //find one
        UserSimpleRequest simpleRequest = UserSimpleRequest.builder()
                .nickname("test")
                .phoneNumber("test")
                .pwd("test")
                .avatarUrl("test")
                .intro("test")
                .loginStatus("test")
                .loginIp("test")
                .accessToken("test")
                .city("test")
                .build();
        ApiResponse findOneResp = controller.findOne(simpleRequest);
        Assert.assertTrue(findOneResp.getCode() == 10000);

        //limit list
        ApiResponse<List<User>> limitListResp = controller.limitList(limitRequest);
        Assert.assertTrue(limitListResp.getCode() == 10000);

        //delete
        ApiResponse deleteResp = controller.delete(id);
        Assert.assertTrue(deleteResp.getCode() == 10000);
    }

}
