package com.yexx.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.yexx.user.UserApplication;
import com.yexx.user.domain.Account;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;

import java.time.LocalDateTime;

/**
 * 账户 service 测试
 *
 * @author code Generator
 * @date 2020-12-02
 */
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserApplication.class)
public class AccountServiceTest {

    @Resource
    public AccountService service;

    @Test
    public void CRUDTest() {
        Account o = Account.builder()
                .name("TEST")
                .username("TEST")
                .pwd("TEST")
                .secret("TEST")
                .pwdStatus(111)
                .phoneNumber("TEST")
                .email("TEST")
                .avatarUrl("TEST")
                .gender(111)
                .idCardNo("TEST")
                .disable(true)
                .registerTime(LocalDateTime.now())
                .build();

        //add
        service.save(o);
        Assert.assertNotNull(o.getId());

        //update
        //service.updateById()

        //get
        o = service.getById(o.getId());
        Assert.assertNotNull(o.getId());

        //page
        Page<Account> page = new Page<>(1, 2, true);
        IPage<Account> oPage = service.page(page);
        //Assert.assertTrue(oPage.getTotal() > 0);
        Assert.assertNotNull(oPage.getTotal() > 0);

        //delete
        service.removeById(o.getId());
        o = service.getById(o.getId());
        Assert.assertNull(o);

    }
        
}
