package com.yexx.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.yexx.user.UserApplication;
import com.yexx.user.domain.User;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 用户 service 测试
 *
 * @author code Generator
 * @date 2020-12-02
 */
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserApplication.class)
public class UserServiceTest {

    @Resource
    public UserService service;

    @Test
    public void CRUDTest() {
        User o = User.builder()
                .accountId(111L)
                .nickname("TEST")
                .phoneNumber("TEST")
                .pwd("TEST")
                .registerTime(LocalDateTime.now())
                .avatarUrl("TEST")
                .birth(LocalDate.now())
                .gender(111)
                .intro("TEST")
                .status(111)
                .loginStatus("TEST")
                .loginTime(LocalDateTime.now())
                .loginIp("TEST")
                .loginTimes(111)
                .accessToken("TEST")
                .inviteUserId(111L)
                .city("TEST")
                .build();

        //add
        service.save(o);
        Assert.assertNotNull(o.getId());

        //update
        //service.updateById()

        //get
        o = service.getById(o.getId());
        Assert.assertNotNull(o.getId());

        //page
        Page<User> page = new Page<>(1, 2, true);
        IPage<User> oPage = service.page(page);
        //Assert.assertTrue(oPage.getTotal() > 0);
        Assert.assertNotNull(oPage.getTotal() > 0);

        //delete
        service.removeById(o.getId());
        o = service.getById(o.getId());
        Assert.assertNull(o);

    }
        
}
