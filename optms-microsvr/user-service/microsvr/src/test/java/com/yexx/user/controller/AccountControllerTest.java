package com.yexx.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yexx.core.entity.ApiResponse;
import com.yexx.user.UserApplication;
import com.yexx.user.request.account.*;
import com.yexx.user.domain.Account;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * 账户 controller 测试
 *
 * @author code Generator
 * @date 2020-12-02
 */
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserApplication.class)
public class AccountControllerTest {

    @Autowired
    public AccountController controller;

    @Test
    public void CRUDTest() {

        AccountCreateRequest createRequest = AccountCreateRequest.builder()
            .name("TEST")
            .username("TEST")
            .pwd("TEST")
            .secret("TEST")
            .pwdStatus(111)
            .phoneNumber("TEST")
            .email("TEST")
            .avatarUrl("TEST")
            .gender(111)
            .idCardNo("TEST")
            .disable(true)
            .build();

        //insert
        ApiResponse<Account> createResp = controller.create(createRequest);
        Assert.assertTrue(createResp.getCode() == 10000);

        //update
        Long id = createResp.getData().getId();
        AccountUpdateRequest updateRequest = AccountUpdateRequest.builder().id(id)
                .name("test")
                .username("test")
                .pwd("test")
                .secret("test")
                .phoneNumber("test")
                .email("test")
                .avatarUrl("test")
                .idCardNo("test")
                .build();
        ApiResponse updateResp = controller.update(updateRequest);
        Assert.assertTrue(updateResp.getCode() == 10000);

        //find by id
        ApiResponse<Account> findByIdResp = controller.findById(id);
        Account dox = findByIdResp.getData();

        Assert.assertTrue("test".equals(dox.getName()));
        Assert.assertTrue("test".equals(dox.getUsername()));
        Assert.assertTrue("test".equals(dox.getPwd()));
        Assert.assertTrue("test".equals(dox.getSecret()));
        Assert.assertTrue("test".equals(dox.getPhoneNumber()));
        Assert.assertTrue("test".equals(dox.getEmail()));
        Assert.assertTrue("test".equals(dox.getAvatarUrl()));
        Assert.assertTrue("test".equals(dox.getIdCardNo()));

        //fond map
        Set<Long> ids = new HashSet<>();
        ids.add(id);
        ApiResponse<Map<Long, Account>> findByIdsResp = controller.findByIds(ids);
        Map map = findByIdsResp.getData();
        Assert.assertTrue(map.containsKey(id));

        //page
        AccountPageRequest pageRequest = AccountPageRequest.builder()
                .name("test")
                .username("test")
                .pwd("test")
                .secret("test")
                .phoneNumber("test")
                .email("test")
                .avatarUrl("test")
                .idCardNo("test")
                .build();
        pageRequest.setPageNum(1);
        pageRequest.setPageSize(10);
        ApiResponse<IPage<Account>> pageResp = controller.page(pageRequest);
        Assert.assertTrue(pageResp.getCode() == 10000);


        AccountLimitRequest limitRequest = AccountLimitRequest.builder()
                .name("test")
                .username("test")
                .pwd("test")
                .secret("test")
                .phoneNumber("test")
                .email("test")
                .avatarUrl("test")
                .idCardNo("test")
                .build();
        limitRequest.setOffset(0);
        limitRequest.setLimit(10);


        //find one
        AccountSimpleRequest simpleRequest = AccountSimpleRequest.builder()
                .name("test")
                .username("test")
                .pwd("test")
                .secret("test")
                .phoneNumber("test")
                .email("test")
                .avatarUrl("test")
                .idCardNo("test")
                .build();
        ApiResponse findOneResp = controller.findOne(simpleRequest);
        Assert.assertTrue(findOneResp.getCode() == 10000);

        //limit list
        ApiResponse<List<Account>> limitListResp = controller.limitList(limitRequest);
        Assert.assertTrue(limitListResp.getCode() == 10000);

        //delete
        ApiResponse deleteResp = controller.delete(id);
        Assert.assertTrue(deleteResp.getCode() == 10000);
    }

}
