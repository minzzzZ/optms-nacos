package com.yexx.nacos.service;

/**
 * Description: 测试service
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/10/14-11:29
 */
public interface StorageService {
    String decr(Integer decr);
}
