package com.yexx.nacos.service.impl;

import com.yexx.nacos.entity.StorageDO;
import com.yexx.nacos.mapper.StorageMapper;
import com.yexx.nacos.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Description: 测试service
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/10/14-11:28
 */
@Service
public class StorageServiceImpl implements StorageService {

    @Autowired
    private StorageMapper storageMapper;

    @Override
    public String decr(Integer decr) {
        StorageDO storageDO = storageMapper.selectById(1);
        int effectRowCount = storageMapper.updateById(StorageDO.builder().id(1L).count((long) (storageDO.getCount().intValue() - decr)).build());
        return "success count: " + effectRowCount;
    }
}
