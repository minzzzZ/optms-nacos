package com.yexx.nacos.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yexx.nacos.entity.StorageDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description: 测试mapper
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/10/14-11:27
 */
@Mapper
public interface StorageMapper extends BaseMapper<StorageDO> {
}
