// Refer to document:  https://github.com/nacos-group/nacos-examples/tree/master/nacos-spring-cloud-example/nacos-spring-cloud-config-example
package com.yexx.nacos.controller;

import com.alibaba.fastjson.JSON;
import com.yexx.nacos.properties.OrderProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config")
public class ConfigController {

    @Autowired
    OrderProperties orderProperties;

    @RequestMapping("/get")
    public String get() {
        return JSON.toJSONString(orderProperties);
    }
}