package com.yexx.nacos;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StopWatch;


/**
 * Description: 启动配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/08/11-15:13
 */
@Slf4j
@SpringBootApplication
public class NacosProviderApplication {

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("run-application");
        SpringApplication.run(NacosProviderApplication.class, args);
        stopWatch.stop();
        log.info("run end！ {}", stopWatch.prettyPrint());
    }
}
