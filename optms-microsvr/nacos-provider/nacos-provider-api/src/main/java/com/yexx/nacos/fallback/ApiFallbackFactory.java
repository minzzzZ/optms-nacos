package com.yexx.nacos.fallback;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Description: 服务发现容错
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/08/12-16:45
 */
@Slf4j
@Component
public class ApiFallbackFactory<T> implements FallbackFactory<T> {

    @Override
    public T create(Throwable throwable) {
        log.error("{}", "进入到容错信息的error打印");
        log.error("{}", throwable);
        throw new RuntimeException(throwable.getMessage());
//        return new DiscoveryApi() {
//            @Override
//            public String get(String serviceName) {
//                return "没有找到服务";
//            }
//
//            @Override
//            public String echo(String name) {
//                return "欢迎你:陌生人";
//            }
//
//            @Override
//            public String decr(Integer decr) {
//                return "fail";
//            }
//        };
    }
}
