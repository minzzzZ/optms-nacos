package com.yexx.nacos.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Description: 测试
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/10/14-11:23
 */
@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("storage")
public class StorageDO {

    private Long id;

    private String commodityCode;

    private Long count;
}
