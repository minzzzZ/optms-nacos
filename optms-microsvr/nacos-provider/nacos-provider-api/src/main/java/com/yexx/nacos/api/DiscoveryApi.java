package com.yexx.nacos.api;

import com.yexx.core.constant.FeignConstants;
import com.yexx.nacos.fallback.ApiFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Description: 服务发现 api
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/08/12-10:15
 */
// todo seata回滚:spring 事务默认只在发生未捕获的RuntimeException()时才进行回滚,被捕获的异常会导致无法回滚
@FeignClient(value = FeignConstants.NACOS_PROVIDER, fallbackFactory = ApiFallbackFactory.class)
public interface DiscoveryApi {

    @GetMapping("/discovery/get")
    String get(@RequestParam(value = "serviceName", required = false) String serviceName);

    @GetMapping("/discovery/echo")
    String echo(@RequestParam(value = "name", required = false) String name);

    @GetMapping("/discovery/decr")
    String decr(@RequestParam(value = "", required = false) Integer decr);
}
