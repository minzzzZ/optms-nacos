package com.yexx.mybatis;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * Description: 公共mapper
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/07-11:16
 */
public interface CommonMapper<T> extends BaseMapper<T> {
    //自定义公用mapper 所有微服务的Mapper集成CommonMapper
}
