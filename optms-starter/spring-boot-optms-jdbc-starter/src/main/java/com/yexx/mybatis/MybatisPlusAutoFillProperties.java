package com.yexx.mybatis;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * Description: mybatis-plus 自动填充属性
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/07-10:57
 */
@Getter
@Setter
@RefreshScope
@ConfigurationProperties(prefix = "optms.mybatis-plus.auto-fill")
public class MybatisPlusAutoFillProperties {

    /**是否开启自动填充*/
    private Boolean enable = true;

    /**是否开启插入填充*/
    private Boolean enableInsertFill = true;

    /**是否开启更新填充*/
    private Boolean enableUpdateFill = true;

    /**创建字段*/
    private String createTimeField = "createTime";

    /**更新字段*/
    private String updateTimeField = "updateTime";
}
