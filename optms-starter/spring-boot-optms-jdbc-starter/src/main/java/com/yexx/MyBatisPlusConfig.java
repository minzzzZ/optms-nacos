package com.yexx;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.yexx.mybatis.DateMetaObjectHandler;
import com.yexx.mybatis.MybatisPlusAutoFillProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Description: mybatis-plus 自动配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/07-11:09
 */
@Configuration
@EnableConfigurationProperties(value = {MybatisPlusAutoFillProperties.class})
public class MyBatisPlusConfig {

    @Autowired
    private MybatisPlusAutoFillProperties autoFillProperties;

    //mybatis-plus 自动填充
    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "optms.mybatis-plus.auto-fill", name = "enable", havingValue = "true", matchIfMissing = true)
    public MetaObjectHandler metaObjectHandler() {
        return new DateMetaObjectHandler(autoFillProperties);
    }

}
