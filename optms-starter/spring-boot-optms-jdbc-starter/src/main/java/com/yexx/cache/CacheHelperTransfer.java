package com.yexx.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author: Guosheng.Zhang
 * @date: 2020/8/10
 */
@Component
public class CacheHelperTransfer {

    @Value("${optms.cache.prefix}")
    private String prefix;

    @Value("${mybatis-plus.configuration.cache-expire:300}")
    private Integer expire;

    @Autowired
    private RedisTemplate redisTemplate;

    @PostConstruct
    public void init() {
        CacheHelper.setRedisTemplate(redisTemplate);
        CacheHelper.setPrefix(prefix + ":mapper");
        CacheHelper.setExpire(expire);
    }

}
