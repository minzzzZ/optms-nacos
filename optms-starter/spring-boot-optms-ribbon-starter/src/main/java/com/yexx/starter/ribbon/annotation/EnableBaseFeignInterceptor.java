package com.yexx.starter.ribbon.annotation;

import com.yexx.starter.ribbon.config.FeignInterceptorConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description: 开启feign拦截器传递数据给下游服务，只包含基础数据
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/07-18:01
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(FeignInterceptorConfig.class)
public @interface EnableBaseFeignInterceptor {
}
 