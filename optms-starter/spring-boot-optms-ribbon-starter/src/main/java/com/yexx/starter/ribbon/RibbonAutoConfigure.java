package com.yexx.starter.ribbon;

import com.yexx.starter.ribbon.properties.RestTemplateProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.ribbon.DefaultPropertiesFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Description: ribbon配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/07-15:17
 */
@Configuration
@EnableConfigurationProperties(RestTemplateProperties.class)
public class RibbonAutoConfigure {

    @Bean
    public DefaultPropertiesFactory defaultPropertiesFactory() {
        return new DefaultPropertiesFactory();
    }
}
