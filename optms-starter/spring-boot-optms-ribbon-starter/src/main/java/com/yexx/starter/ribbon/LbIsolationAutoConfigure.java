package com.yexx.starter.ribbon;

import com.yexx.starter.ribbon.irule.RuleConfigure;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.netflix.ribbon.RibbonClients;

/**
 * Description: 自定义负载均衡策略
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/07-16:36
 */
@ConditionalOnProperty(name = "optms.ribbon.isolation.enable", havingValue = "true")
@RibbonClients(defaultConfiguration = {RuleConfigure.class})
public class LbIsolationAutoConfigure {
}
