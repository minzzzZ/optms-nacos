package com.yexx.starter.ribbon.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description: restTemplate配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/07-16:18
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "optms.rest-template")
public class RestTemplateProperties {

    /**
     * 最大链接数
     */
    private int maxTotal = 200;
    /**
     * 同路由最大并发数
     */
    private int maxPerRoute = 50;
    /**
     * 读取超时时间 ms
     */
    private int readTimeout = 35000;
    /**
     * 链接超时时间 ms
     */
    private int connectTimeout = 10000;
}
