package com.yexx.starter.ribbon;

import feign.Feign;
import feign.Logger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;

/**
 * Description: feign 配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/07-15:34
 */
@ConditionalOnClass(Feign.class)
public class FeignAutoConfigure {

    /** feign 日志全局配置 */
    @Bean
    Logger.Level feignLogger(){
        return Logger.Level.FULL;
    }
}
