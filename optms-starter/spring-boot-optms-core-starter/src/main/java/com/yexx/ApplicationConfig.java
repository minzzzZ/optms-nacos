package com.yexx;

import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

/**
 * Description: 应用启动配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/08/12-09:32
 */
@Configuration
@EnableFeignClients
@EnableCircuitBreaker
@EnableDiscoveryClient
public class ApplicationConfig {

}