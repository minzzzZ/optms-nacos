package com.yexx.logger;

import com.yexx.logger.properties.LoggerProperties;
import com.yexx.logger.properties.TraceProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Configuration;

/**
 * Description: 日志配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/08/12-14:34
 */
@Configuration
@ConditionalOnProperty(prefix = "optms", name = "logger.enable", havingValue = "true", matchIfMissing = true)
@EnableConfigurationProperties(value = {TraceProperties.class, LoggerProperties.class})
public class LoggerConfig extends CachingConfigurerSupport {
}
