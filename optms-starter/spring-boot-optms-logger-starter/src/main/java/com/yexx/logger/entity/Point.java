package com.yexx.logger.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Description: 监控对象
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/04-16:14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Point implements Serializable {

    private static final long serialVersionUID = -177075924735679072L;

    //请求id
    private Object requestId;

    //请求类型
    private String type;

    //设备信息 ip=xxx&browser=xxx&os=xxx
    private String device;

    //时间戳
    private LocalDateTime timestamp;

    //应用名
    private String application;

    //类名
    private String className;

    //方法名
    private String methodName;

    //用户id
    private Object userId;

    //参数 json串
    private String params;

    //code
    private Integer code;

    //msg
    private String msg;
}
