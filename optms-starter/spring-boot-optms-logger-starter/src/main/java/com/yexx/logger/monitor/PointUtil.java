package com.yexx.logger.monitor;

import com.yexx.logger.entity.Point;
import lombok.extern.slf4j.Slf4j;

import java.time.format.DateTimeFormatter;

/**
 * Description: 日志埋点工具类
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/08/12-14:34
 */
@Slf4j
public class PointUtil {

    private static final String MSG_PATTERN = "{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}";

    public void info(Point point) {
        log.info(MSG_PATTERN,
                point.getTimestamp().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")),
                point.getApplication(), point.getRequestId(), point.getClassName(), point.getMethodName(),
                point.getUserId(), point.getParams());
    }

    /**
     * 格式为：{时间}|{应用名}|{请求id}|{统计类型}|{设备信息}|{类名}|{方法名}|{用户id}|{参数json}|{异常码}|{异常信息}
     * 2020-12-04 18:34:15.352|user-service|1607078055338|com.yexx.user.controller.UserController|findOne|null|[{"nickname":"如果爱有天意3"}]|null|null
     */
    //正常debug
    public static void debug(Point point) {
        log.debug(MSG_PATTERN,
                point.getTimestamp().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")),
                point.getApplication(), point.getRequestId(), point.getType(), point.getDevice(), point.getClassName(), point.getMethodName(),
                point.getUserId(), point.getParams(), point.getCode(), point.getMsg());
    }

    /**
     * 格式为：{时间}|{应用名}|{请求id}|{类名}|{方法名}|{用户id}|{参数json}|{异常码}|{异常信息}
     * 2020-12-04 18:34:18.118|user-service|1607078055338|com.yexx.user.controller.UserController|findOne|null|null|31001|查询结果不唯一
     */
    //对异常的debug
    public static void debugExp(Point point) {
        log.debug(MSG_PATTERN,
                point.getTimestamp().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")),
                point.getApplication(), point.getRequestId(), point.getType(), point.getDevice(),point.getClassName(), point.getMethodName(),
                point.getUserId(), point.getParams(), point.getCode(), point.getMsg());
    }

    public static void warn(Point point) {
        log.warn(MSG_PATTERN,
                point.getTimestamp().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")),
                point.getApplication(), point.getRequestId(), point.getType(), point.getDevice(), point.getClassName(), point.getMethodName(),
                point.getUserId(), point.getParams());
    }

    public static void error(Point point) {
        log.error(MSG_PATTERN,
                point.getTimestamp().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")),
                point.getApplication(), point.getRequestId(), point.getType(), point.getDevice(), point.getClassName(), point.getMethodName(),
                point.getUserId(), point.getParams());
    }
}
