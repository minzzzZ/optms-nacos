package com.yexx.logger;

import org.slf4j.TtlMDCAdapter;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Description: 初始化TtlMDCAdapter实例，并替换MDC中的adapter对象
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/02-18:14
 */
public class TtlMDCAdapterInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        //加载TtlMDCAdapter实例
        TtlMDCAdapter.getInstance();
    }
}
