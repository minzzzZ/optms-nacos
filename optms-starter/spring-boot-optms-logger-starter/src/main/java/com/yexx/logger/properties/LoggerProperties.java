package com.yexx.logger.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description:
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/08-10:20
 */
@ConfigurationProperties(value = "optms.logger")
public class LoggerProperties {

    /** 默认开启 */
    private Boolean enable = true;

}
