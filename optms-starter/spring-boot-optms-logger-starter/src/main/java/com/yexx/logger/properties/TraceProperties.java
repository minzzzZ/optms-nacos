package com.yexx.logger.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * Description: 日志追踪配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/07-11:51
 */
@Getter
@Setter
@RefreshScope
@ConfigurationProperties(value = "optms.trace")
public class TraceProperties {

    /** 是否开启日志链路追踪 */
    private Boolean enable = false;
}
