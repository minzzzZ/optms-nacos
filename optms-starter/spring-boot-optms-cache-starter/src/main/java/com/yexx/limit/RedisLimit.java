package com.yexx.limit;

import com.yexx.utils.ScriptUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.util.ArrayList;
import java.util.List;

/**
 * redis 请求限流
 *
 * @author zuomin(MylesZelic@outlook.com)
 * @date 2020/04/11 18:33
 */
@Slf4j
public class RedisLimit {

    private final StringRedisTemplate redisTemplate;
    private final int limit;
    private static final int FAIL_CODE = 0;

    /**
     * lua script
     */
    private String script;

    private RedisLimit(Builder builder) {
        this.limit = builder.limit;
        this.redisTemplate = builder.redisTemplate;
        buildScript();
    }


    /**
     * limit traffic
     *
     * @param prefix 前缀
     * @return if true
     */
    public boolean limit(String prefix) {

        Object result = limitRequest(prefix);

        return FAIL_CODE != (Long) result;
    }

    private Object limitRequest(String prefix) {
        String key = prefix + System.currentTimeMillis() / 1000;
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>(script, Long.class);
        List<String> keys = new ArrayList<>();
        keys.add(key);
        String[] args = {String.valueOf(limit)};
        return redisTemplate.execute(redisScript, keys, args);
    }

    /**
     * read lua script
     */
    private void buildScript() {
        script = ScriptUtil.getScript("limit.lua");
    }


    /**
     * the builder
     */
    public static class Builder {
        private final StringRedisTemplate redisTemplate;

        private int limit = 200;

        public Builder(StringRedisTemplate redisTemplate) {
            this.redisTemplate = redisTemplate;
        }

        public Builder limit(int limit) {
            this.limit = limit;
            return this;
        }

        public RedisLimit build() {
            return new RedisLimit(this);
        }

    }
}
