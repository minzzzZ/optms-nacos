package com.yexx.limit.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 限流配置
 *
 * @author zuomin(MylesZelic@outlook.com)
 * @date 2020/04/11 18:33
 */
@Getter
@Setter
@ConfigurationProperties("optms.redis.limit")
public class RedisLimitProperties {

    /**
     * 是否启用限流
     */
    private Boolean enable;

    /**
     * 具体限流value
     */
    private int value;
}
