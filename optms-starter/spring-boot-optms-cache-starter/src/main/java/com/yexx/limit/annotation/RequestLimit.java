package com.yexx.limit.annotation;

import java.lang.annotation.*;

/**
 * redis请求限流 Controller注解
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date 2020/08/14 11:13
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestLimit {

    /**
     * Error code
     * @return
     * code
     */
    int errorCode() default 40100;

    /**
     * Error Message
     * @return
     * message
     */
    String errorMsg() default "request limited";
}
