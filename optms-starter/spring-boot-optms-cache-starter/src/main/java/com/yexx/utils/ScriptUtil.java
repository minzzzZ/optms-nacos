package com.yexx.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Description: script工具类
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date 2020/08/14 11:13
 */
@Slf4j
public class ScriptUtil {

    /**
     * return lua script String
     *
     * @param path 路劲
     * @return lua string
     */
    public static String getScript(String path) {
        StringBuilder sb = new StringBuilder();
        InputStream stream = ScriptUtil.class.getClassLoader().getResourceAsStream(path);
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        try {
            String str;
            while ((str = br.readLine()) != null) {
                sb.append(str).append(System.lineSeparator());
            }
        } catch (IOException e) {
            log.error("read lua script string failed.", e);
        }
        return sb.toString();
    }
}
