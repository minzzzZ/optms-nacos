package com.yexx;

import com.yexx.config.DistributedLockerConfig;
import com.yexx.config.RedisConfig;
import com.yexx.config.IdGeneratorConfig;
import com.yexx.config.RedisLimitConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * CacheConfig 缓存配置类
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date 2020/04/11 18:33
 */
@Configuration
@Import({RedisConfig.class, DistributedLockerConfig.class, IdGeneratorConfig.class, RedisLimitConfig.class})
public class CacheConfig {

}
