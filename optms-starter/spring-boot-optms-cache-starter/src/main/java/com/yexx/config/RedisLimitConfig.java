package com.yexx.config;

import com.yexx.limit.RedisLimit;
import com.yexx.limit.intercept.SpringMvcIntercept;
import com.yexx.limit.properties.RedisLimitProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * redis 请求限流自动配置
 *
 * @author zuomin(MylesZelic@outlook.com)
 * @date 2020/04/11 18:33
 */
@Configuration
@EnableConfigurationProperties(RedisLimitProperties.class)
@ConditionalOnProperty(name = "optms.redis.limit.enable", havingValue = "true")
public class RedisLimitConfig implements WebMvcConfigurer {


    @Autowired
    private RedisLimitProperties limitProperties;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Bean
    public RedisLimit redisLimit() {
        return new RedisLimit.Builder(redisTemplate)
                .limit(limitProperties.getValue())
                .build();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SpringMvcIntercept(redisLimit()));
    }
}
