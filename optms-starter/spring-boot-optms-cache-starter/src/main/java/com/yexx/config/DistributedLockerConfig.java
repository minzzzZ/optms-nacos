package com.yexx.config;

import com.yexx.lock.*;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;
import org.redisson.config.SentinelServersConfig;
import org.redisson.config.SingleServerConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;

/**
 * Description: Distributed Locker Config
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date 2020/04/04 18:17
 */
@Configuration
@ConditionalOnClass(RedissonDistributedLocker.class)
@EnableConfigurationProperties(RedissonProperties.class)
@ConditionalOnProperty(prefix = "spring.redis", name = "host")
public class DistributedLockerConfig {

    /**
     * Description: 单机模式
     *
     * @return RedissonClient
     */
    @Bean
    public RedissonClient redissonSingle(RedissonProperties redissonProperties) {
        Config config = new Config();
        SingleServerConfig serverConfig = config.useSingleServer()
                .setAddress("redis://" + redissonProperties.getHost() + ":" + redissonProperties.getPort())
                .setTimeout(redissonProperties.getTimeout())
                .setDatabase(redissonProperties.getDatabase());
        if (!StringUtils.isEmpty(redissonProperties.getPassword())) {
            serverConfig.setPassword(redissonProperties.getPassword());
        }
        return Redisson.create(config);
    }

    /**
     * Description: 哨兵模式
     *
     * @return RedissonClient
     */
    public RedissonClient redissonSentinel(RedissonProperties redissonProperties) {
        Config config = new Config();
        SentinelServersConfig sentinelServersConfig = config.useSentinelServers().addSentinelAddress(
                "redis://106.53.240.230:30377", "redis://106.53.240.230:30378", "redis://106.53.240.230:30379")
                .setMasterName("mymaster")
                .setTimeout(redissonProperties.getTimeout())
                .setDatabase(redissonProperties.getDatabase());
        if (!StringUtils.isEmpty(redissonProperties.getPassword())) {
            sentinelServersConfig.setPassword(redissonProperties.getPassword());
        }
        return Redisson.create(config);
    }

    /**
     * Description: 集群模式
     *
     * @return RedissonClient
     */
    public RedissonClient redissonCluster(RedissonProperties redissonProperties) {
        Config config = new Config();
        ClusterServersConfig clusterServersConfig = config.useClusterServers().addNodeAddress(
                "redis://172.29.3.245:6375", "redis://172.29.3.245:6376", "redis://172.29.3.245:6377",
                "redis://172.29.3.245:6378", "redis://172.29.3.245:6379", "redis://172.29.3.245:6380")
                .setScanInterval(5000)
                .setTimeout(redissonProperties.getTimeout());
        if (!StringUtils.isEmpty(redissonProperties.getPassword())) {
            clusterServersConfig.setPassword(redissonProperties.getPassword());
        }
        return Redisson.create(config);
    }

    /**
     * 装配locker类 redisson
     *
     * @return DistributedLocker
     */
    @Bean
    public DistributedLocker distributedLocker(RedissonClient redissonSingle) {
        RedissonDistributedLocker locker = new RedissonDistributedLocker();
        locker.setRedissonClient(redissonSingle);
        return locker;
    }

    /**
     * 装配locker类 lua 测试未通过
     *
     * @return LuaDistributedLocker
     */
    //@Bean
    public LuaDistributedLocker luaDistributedLocker(StringRedisTemplate redisTemplate) {
        RedisLuaDistributionLocker locker = new RedisLuaDistributionLocker(redisTemplate);
        return locker;
    }


}