package com.yexx.config;

import com.yexx.idGenerator.IncrIdGenerator;
import com.yexx.uid.IdGenerator;
import com.yexx.uid.SnowFlakeIdGenerator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Description: id自增主键配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/08/13-17:35
 */
@Configuration
@ConditionalOnProperty(prefix = "spring.redis", name = "host")
public class IdGeneratorConfig {

    @Bean
    public IdGenerator incrIdGenerator() {
        return new IncrIdGenerator();
    }

    @Bean
    @ConditionalOnMissingBean
    public IdGenerator idGenerator() {
        return new SnowFlakeIdGenerator(1, 1);
    }
}
