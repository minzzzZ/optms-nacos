package com.yexx.lock;

import java.util.concurrent.TimeUnit;

/**
 * @author zuomin
 */
public interface DistributedLocker {
 
    boolean lock(String lockKey);
 
    boolean unlock(String lockKey);

    boolean lock(String lockKey, int timeout);

    boolean lock(String lockKey, TimeUnit unit, int timeout);
}