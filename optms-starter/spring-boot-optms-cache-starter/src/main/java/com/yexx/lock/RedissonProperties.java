package com.yexx.lock;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zuomin
 */
@ConfigurationProperties(prefix = "spring.redis")
public class RedissonProperties {

    private Integer timeout;

    private Integer port;

    private Integer database;

    private String host;

    private String password;


    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getDatabase() {
        return database;
    }

    public void setDatabase(Integer database) {
        this.database = database;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}