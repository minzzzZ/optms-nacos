package com.yexx.lock;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description: LockContent
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/04/24-16:17
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LockContent {

    /*开始时间:ms*/
    private Long startTime;

    /*加锁时间:ms*/
    private Long lockExpire;

    /*过期时间(传入):ms*/
    private Long expireTime;

    /*全局请求id*/
    private String requestId;

    /*当前线程:currentThread*/
    private Thread thread;

    /*业务过期时间:ms*/
    private Long bizExpire;

    /*锁定次数:次*/
    private Integer lockCount;
}
