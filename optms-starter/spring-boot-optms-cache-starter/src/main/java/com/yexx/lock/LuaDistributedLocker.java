package com.yexx.lock;

/**
 * Description: lua redis 锁
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/08/14-11:28
 */
public interface LuaDistributedLocker {

    boolean lock(String lockKey, String requestId, long expire);

    boolean unlock(String lockKey, String lockValue);
}
