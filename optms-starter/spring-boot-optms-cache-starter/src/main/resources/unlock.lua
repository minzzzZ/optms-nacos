if redis.call('get', KEYS[1]) == ARGV[1] then
    local ctime = tonumber(ARGV[2])
    local biz_timeout = tonumber(ARGV[3])
    if ctime > 0 then
        if redis.call('exists', KEYS[2]) == 1 then
            local avg_time = redis.call('get', KEYS[2])
            avg_time = (tonumber(avg_time) * 8 + ctime * 2) / 10
            if avg_time >= biz_timeout - 5 then redis.call('set', KEYS[2], avg_time, 'EX', 24 * 60 * 60)
            else redis.call('del', KEYS[2])
            end
        elseif ctime > biz_timeout - 5 then redis.call('set', KEYS[2], ARGV[2], 'EX', 24 * 60 * 60)
        end
    end
    return redis.call('del', KEYS[1])
else return 0
end