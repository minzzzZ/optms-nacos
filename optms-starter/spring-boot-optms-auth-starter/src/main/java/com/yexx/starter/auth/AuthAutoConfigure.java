package com.yexx.starter.auth;

import org.springframework.context.annotation.Configuration;

/**
 * Description: 启动项
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/25-15:16
 */
@Configuration
public class AuthAutoConfigure {
}
