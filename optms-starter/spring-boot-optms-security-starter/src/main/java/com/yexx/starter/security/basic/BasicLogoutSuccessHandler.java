package com.yexx.starter.security.basic;

import com.yexx.starter.security.basic.constant.SecurityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Description: 登出handler
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/10/30-17:45
 */
public class BasicLogoutSuccessHandler implements LogoutSuccessHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        logger.info("{}, logout success!", authentication.getPrincipal());
        httpServletResponse.setContentType("application/json;charset=utf-8");
        httpServletResponse.setCharacterEncoding("utf-8");
        try (PrintWriter out = httpServletResponse.getWriter()) {
            out.write(SecurityConstants.RETURN_CODE.LOGOUT_SUCCESS);
            out.flush();
        }
    }
}
