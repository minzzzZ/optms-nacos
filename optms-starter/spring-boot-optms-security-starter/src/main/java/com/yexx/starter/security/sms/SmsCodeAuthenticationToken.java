package com.yexx.starter.security.sms;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Description: 短信验证码认证token
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/11/03 14:16
 */
public class SmsCodeAuthenticationToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = 2383092775910246006L;

    private String deviceUuid;
    private final String phoneNum;
    private String smsCode;
    private String osName;
    private String osVersion;

    public SmsCodeAuthenticationToken(String phoneNum, String smsCode, String deviceUuid, String osName, String osVersion) {
        super(null);
        this.phoneNum = phoneNum;
        this.smsCode = smsCode;
        this.deviceUuid = deviceUuid;
        this.osName = osName;
        this.osVersion = osVersion;
        setAuthenticated(false);
    }

    public SmsCodeAuthenticationToken(String phoneNum, String smsCode, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.phoneNum = phoneNum;
        this.smsCode = smsCode;
        super.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return this.smsCode;
    }

    @Override
    public Object getPrincipal() {
        return this.phoneNum;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException("Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        }
        super.setAuthenticated(false);
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        this.smsCode = null;
    }

    public String getDeviceUuid() {
        return deviceUuid;
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }

    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

}
