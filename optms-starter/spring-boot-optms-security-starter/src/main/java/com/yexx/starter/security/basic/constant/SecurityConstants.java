package com.yexx.starter.security.basic.constant;

/**
 * Description: 常量
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/10/30-10:22
 */
@SuppressWarnings("ALL")
public final class SecurityConstants {

    // 认证请求头
    public final static String TOKEN_HEADER = "Authorization";
    public final static String TOKEN_PARAM = "access_token";
    // token前缀
    public final static String HEAD = "Bearer ";

    public static final String LOGIN_FAIL = "{ \"code\": 40008, \"message\": \"%s\" }";
    public static final String LOGIN_SUCCESS = "{ \"code\": 10000, \"data\": { \"token\": \"%s\", \"expire\": %d } }";


    public static final class PATH {
        public final static String LOGIN_PHONE_NUM_PWD = "/api/login/phone_num/pwd";
        public final static String LOGIN_PHONE_NUM_SMS = "/api/login/phone_num/sms";
        public final static String LOGIN_ERROR = "/api/login?error=true";
        public final static String LOGIN_AUTH = "/api/login/auth";
        public final static String LOGOUT = "/api/logout";
    }

    public static final class RETURN_CODE {
        public final static Integer LOGOUT_SUCCESS = 10000;     //登录成功
        public final static Integer MISS_TOKEN = 40001;         //缺失token
        public final static Integer MISS_BEARER = 40002;        //缺失bearer
        public final static Integer INVALID_TOKEN = 40003;      //非法token
        public final static Integer ERROR_TOKEN = 40004;        //错误token
        public final static Integer EMPTY_SUBJECT = 40005;      //空的subject
        public final static Integer TOKEN_EXPIRE = 40006;       //token超时
        public final static Integer SIGN_IN_ELSEWHERE = 40007;  //其他地方登录
        public final static Integer LOGIN_FAIL = 40008;         //登录失败
        public final static Integer DENIED_CODE = 40009;        //无权限
        public final static Integer DEVICE_UNBIND = 40010;      //设备未绑定
        public final static Integer PATH_NOT_FOUND = 40011;     //api路径未找到
        public final static Integer OUT_OF_LIMIT = 40100;       //限流
        public final static Integer SYS_ERROR = 50000;          //系统错误
    }

    public static final class RETURN_JSON {
        public final static String DENIED_CODE = "{\"code\": 40009, \"message\": \"Access Denied - 无权限\"}";
        public final static String LOGOUT_SUCCESS = "{\"code\": 10000, \"message\": \"LOGOUT SUCCESS\" }";
        public final static String DYNAMIC_CODE = "{\"code\": %s, \"message\": \"%s\"}";
    }

    public static final class PARAMETER{
        public static final String REQUEST_ID = "request_id";
        public static final String LOGIN_SMS_YZN = "sms_yzm";    // 登录时存储短信验证码到缓存中的key

        // 手机号码登陆
        public static final String LOGIN_PHONE_NUM = "phone_num";
        public static final String LOGIN_PWD = "pwd";
        public static final String LOGIN_SMS_CODE = "sms_code";

        // 二维码登陆
        public static final String LOGIN_AUTH_CODE = "authCode";
        public static final String LOGIN_QR_CODE = "qrCode";

        //Device-Uuid
        public static final String DEVICE_UUID = "Device-Uuid";

    }

    public static final class CACHE{
        public static final String LOGIN_IMAGE_YZM = "login:code:image";   // 登录时存储图形验证码到缓存中的key
        public static final String LOGIN_SMS_YZM = "sms:code:login";   // 登录时存储短信验证码到缓存中的key
        public static final String LOGIN_QR_CODE = "qrCode";   // 登录时存储授权码到缓存中的key
        public final static int LOGIN_IMAGE_CODE_EXPIRE = 180;  // 图形验证码的过期时间
    }
}
