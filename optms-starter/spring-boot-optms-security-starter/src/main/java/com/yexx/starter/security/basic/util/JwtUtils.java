package com.yexx.starter.security.basic.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Calendar;
import java.util.Date;

public class JwtUtils {

    /**
     * 根据token获取用户信息
     */
    public static Claims getClaim(String token){
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey("abcdefg")
                    .parseClaimsJws(token).getBody();
            return claims;
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 设置用户信息进jwt
     */
    public static String setClaim(String subject, Integer exp){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, exp);
        String token = Jwts
                .builder()
                .setSubject(subject)
                .setIssuedAt(new Date())
                .setExpiration(calendar.getTime())
                .signWith(SignatureAlgorithm.HS256, "abcdefg")
                .compact();
        return token;
    }

}
