//package com.yexx.starter.security;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.cors.CorsConfiguration;
//import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
//import org.springframework.web.filter.CorsFilter;
//
///**
// * Description: CORS 配置 : 在springboot环境下也可以生效,spring security推荐不推荐
// *
// * @author: zuomin (myleszelic@outlook.com)
// * @date: 2020/10/29-10:31
// */
//@Configuration
//public class CorsConfig {
//
//    /*请求拦截不限，解决跨域的问题*/
//    private CorsConfiguration buildConfig() {
//        CorsConfiguration config = new CorsConfiguration();
//        config.setAllowCredentials(true);
//        config.addAllowedOrigin("*");
//        config.addAllowedHeader("*");
//        config.addAllowedMethod("*");
//        return config;
//    }
//
//    @Bean
//    public CorsFilter corsFilter() {
//        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**", buildConfig());
//        return new CorsFilter(source);
//    }
//
//}
