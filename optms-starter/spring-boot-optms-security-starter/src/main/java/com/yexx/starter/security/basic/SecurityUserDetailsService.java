package com.yexx.starter.security.basic;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Description: 自定义 UserDetailsService 扩展功能
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/10/29-10:54
 */
public interface SecurityUserDetailsService extends UserDetailsService {

    UserDetails loadUserByUsername(String username);

    /*在loadUserByUsername()方法的基础上扩展更多的方法,例如:绑定设备,记录登录日志等*/

    default void loginSuccessful(HttpServletRequest request, HttpServletResponse response, Authentication authResult) {

    }

    default Boolean checkBindDevice(String phoneNumber, String deviceUuid) {
        return true;
    }

    default Boolean bindDevice(String phoneNumber, String deviceUuid, String osName, String osVersion) {
        return true;
    }
}
