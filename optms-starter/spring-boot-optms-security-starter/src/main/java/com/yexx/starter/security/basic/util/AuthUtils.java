package com.yexx.starter.security.basic.util;

public final class AuthUtils {

    public final static int expire = 60 * 24 * 7;
    public final static String TOKEN_TMP = "token:{userId}:{token}";

    public static String getToken(String userId){
        return JwtUtils.setClaim(userId, 10000000);
    }

    public static String getTokenKey(String userId, String token){
        return TOKEN_TMP.replace("{userId}", userId).replace("{token}", token);
    }

}
