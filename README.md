# optms-cloud

<p align="center">
  <img src="https://img.shields.io/badge/Spring%20Boot-2.3.0.RELEASE-blue" alt="Downloads"/>
  <img src="https://img.shields.io/badge/Spring%20Cloud-Hoxton.SR3-blue" alt="Downloads"/>
  <img src="https://img.shields.io/badge/Spring%20Cloud%20Alibaba-2.2.1.RELEASE-blue" alt="Downloads"/>
  <img src="https://img.shields.io/badge/Elasticsearch-7.6.2-brightgreen" alt="Downloads"/>
</p>

# 介绍
* 基于 SpringBoot 2.3.0.REALEASE+SpringCloud Hoxton.SR3+SpringCloud Alibaba2.2.1.RELEASE
* 打造手到擒来的分布式微服务开发框架

# 版本
* nacos server v1.3.2
* seata server v1.3.0
* sentinel server v1.8.0
* elasticsearch server v7.6.2
* redis server v6.0.4

# 项目结构
``` 
optms-cloud
├── optms-apisvr                                对外服务层
    ├── dianzan-apisvr                          点赞APP对外服务-test
    ├── nacos-consumer                          基于nacos的consumer服务
├── optms-common                                工具类及通用代码
    ├── optms-core                              核心依赖
    ├── optms-uid                               全局id生成
├── optms-gateway                               gateway网关
├── optms-generator                             代码生成器:Ruoyi
├── optms-monitor                               服务监控
    ├── admin-actuator                          springboot-admin服务
├── optms-search                                es搜索服务 v7.6.2
├── optms-microsvr                              微服务层
    ├── nacos-provider                          基于nacos的provider服务
        ├── nacos-provider-api                  provider api接口层
        ├── nacos-provider-entity               provider entity实体层
        ├── nacos-provider-microsvr             provider服务层
    ├── user-service                            用户服务
            ├── api                             用户服务 api接口层
            ├── entity                          用户服务 entity实体层
            ├── microsvr                        用户服务 微服务实现层
├── optms-starter                               启动层
    ├── spring-boot-optms-cache-starter         缓存启动项
    ├── spring-boot-optms-core-starter          核心启动项，核心配置
    ├── spring-boot-optms-jdbc-starter          数据库连接配置
    ├── spring-boot-optms-logger-starter        日志启动项
    ├── spring-boot-optms-ribbon-starter        ribbon负载均衡
    ├── spring-boot-optms-security-starter      security登录授权启动项
    ├── spring-boot-optms-session-starter       session启动项:未完待续
    ├── spring-boot-optms-swagger-starter       swagger启动项 基于:knife4j:2.0.5
├── optms-auth2                                 后续计划：认证授权服务
├── optms-demo                                  后续计划：示例
```
# 服务监控
## SpringBoot-admin
![](./doc/img/admin-server.jpg)
# 接口文档
## 基于snife4j的swagger文档
![](./doc/img/snife-doc.jpg)
# 登录验权
## 手机号验证码登录
![](./doc/img/sms-login.jpg)
## 手机号密码登录
![](./doc/img/pwd-login.jpg)
请求参数
``` http request
curl -i -X POST \
   -H "Content-Type:application/x-www-form-urlencoded" \
   -d "phone_num=9999" \
   -d "pwd=1234abcd" \
 'http://localhost:8887/api/login/phone_num/pwd'
```
返回结果:
``` json
{
    "code": 10000,
    "data": {
        "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3IiwiaWF0IjoxNjA2Mzc5NjI0LCJleHAiOjIyMDYzNzk2MjR9.4efVHdwquEVdUAV3gjbbq6kKLZaYqk73bthQ7vVuwgY",
        "expire": 1606984424076
    }
}
```
## 验权测试
```http request
curl -i -X GET \
   -H "Content-Type:application/json" \
   -H "Authorization:Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3IiwiaWF0IjoxNjA2Mzc5NjI0LCJleHAiOjIyMDYzNzk2MjR9.4efVHdwquEVdUAV3gjbbq6kKLZaYqk73bthQ7vVuwgY" \
 'http://localhost:8887/api/discovery/get?serviceName=nacos-provider'
```
返回结果:
```text
欢迎你:123
```
![](./doc/img/get-api.jpg)
