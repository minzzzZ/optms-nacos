# 说明
* 代码生成器采用Ruoyi的开源代码生成工具,在基础上做了自定义的修改..
# Gitee地址
* [optms代码生成器地址](https://gitee.com/minzzzZ/ry-redis/tree/optms-mybatisplus-generator/)
* 登录默认用户名密码 进入之后点击菜单的系统工具 -> 代码生成-库名(和项目目录有一定关系)
# FAQ
* 现阶段需要自己连接自己的数据库,然后本地启动服务 具体在application-druid.yml中修改数据库地址,需要增加动态数据库还需要了解Ruoyi的项目,现在不做具体的说明,后续会补充.
* 计划部署到线上,但是涉及到自定义的库表的时候不知道怎么处理?暂时搁置.
* 代码生成器和项目架构有很大关系,此代码生成器只对当前optms-cloud有效.