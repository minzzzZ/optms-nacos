package com.yexx.core.entity;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yexx.core.entity.dto.Query;

/**
 * @author: Guosheng.Zhang
 * @date: 2020/8/10
 */
public abstract class SimpleRequest extends Query {

    /**
     * 转换成查询参数
     *
     * @param <T> 查询参数格式对象
     * @return 查询参数
     */
    public <T> QueryWrapper<T> toQueryWapper() {
        return toParamsQueryWapper();
    }

    public abstract <T> QueryWrapper<T> toParamsQueryWapper();

}
