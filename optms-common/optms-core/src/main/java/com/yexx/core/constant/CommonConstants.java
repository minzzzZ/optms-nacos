package com.yexx.core.constant;

/**
 * Description: 公共常量
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/11/06-11:49
 */
@SuppressWarnings("ALL")
public final class CommonConstants {

    public final static class PrefixConstant {
        /** request api prefix */
        public final static String API_PREFIX = "/api";
    }

    public static final class RETURN_CODE {

        /** success code */
        public final static Integer SYS_SUCCESS = 10000;        //成功code

        public final static Integer PARAM_ERROR = 20000;        //业务级别异常码,例如:参数为空,参数不符合要求,查询数据未找到 20001 ~ 29999

        public final static Integer RESERVE_CODE = 30000;       //预留异常码 30001 ~ 39999

        public final static Integer TOKEN_CODE = 40000;         //框架级别异常码,例如:token异常,sentinel限流,redis限流等 40001 ~ 49999

        /** 认证授权使用,不允许修改,不允许复用 */
        public final static Integer MISS_TOKEN = 40001;         //缺失token
        public final static Integer MISS_BEARER = 40002;        //缺失bearer
        public final static Integer INVALID_TOKEN = 40003;      //非法token
        public final static Integer ERROR_TOKEN = 40004;        //错误token
        public final static Integer EMPTY_SUBJECT = 40005;      //空的subject
        public final static Integer TOKEN_EXPIRE = 40006;       //token超时
        public final static Integer SIGN_IN_ELSEWHERE = 40007;  //其他地方登录
        public final static Integer LOGIN_FAIL = 40008;         //登录失败
        public final static Integer DENIED_CODE = 40009;        //无权限
        public final static Integer DEVICE_UNBIND = 40010;      //设备未绑定
        public final static Integer PATH_NOT_FOUND = 40011;     //api路径未找到

        /** sentinel exception return code, do not override */
        public final static Integer BLOCK_EXCEPTION = 40050;        // BlockException 异常接口，其子类为Sentinel五种规则异常的实现类
        public final static Integer AUTHORITY_EXCEPTION = 40051;    // AuthorityException 授权异常
        public final static Integer DEGRADE_EXCEPTION = 40052;      // DegradeException 降级异常
        public final static Integer FLOW_EXCEPTION = 40053;         // FlowException 限流异常
        public final static Integer PARAM_FLOW_EXCEPTION = 40054;   // ParamFlowException 参数限流异常
        public final static Integer SYSTEM_BLOCK_EXCEPTION = 40055; // SystemBlockException 系统负载异常

        /** redis限流 */
        public final static Integer OUT_OF_LIMIT = 40100;       //redis限流

        /** system error code */
        public final static Integer SYS_ERROR = 50000;          //系统级别异常码

    }

    public static final class LOG {
        /**
         * 日志链路追踪id信息头
         */
        public static final String TRACE_ID_HEADER = "x-traceid-header";
        public static final String TRACE_ID_ATTR = "x-traceid-attr";
        /**
         * 日志链路追踪id日志标志
         */
        public static final String LOG_TRACE_ID = "traceId";

        /**
         * 日志追踪 userId
         */
        public static final String LOG_USER_ID = "userId";
        /**
         * 请求id
         */
//        public static final String REQUEST_ID_KEY = "x-requestid-attr";
        /**
         * userid请求头 不要那么明显
         */
        public static final String USER_ID_HEADER = "x-u-header";
        public static final String USER_ID_ATTR = "x-u-attr";

    }

    public static final class RIBBON {
        /**
         * 负载均衡策略-版本号 信息头
         */
        public static final String LB_VERSION_HEADER = "x-lb-version-header";
        /**
         * 注册中心元数据 版本号
         */
        public static final String METADATA_VERSION = "version";
        /**
         * 是否开启自定义隔离规则
         */
        public static final String CONFIG_RIBBON_ISOLATION_ENABLED = "optms.ribbon.isolation.enable";
    }

    public static final class FORMAT {

        public static final String DATE_FORMAT = "yyyy-MM-dd";

        public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

        public static final String TIME_ZONE_GMT8 = "GMT+8";
    }

}
