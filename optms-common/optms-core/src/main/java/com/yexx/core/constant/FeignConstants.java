package com.yexx.core.constant;

/**
 * Description: feign常量
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/08/12-10:21
 */
public final class FeignConstants {

    //nacos provider
    public final static String NACOS_PROVIDER = "nacos-provider";

    //用户服务
    public final static String USER_SERVICE = "user-service";
}
