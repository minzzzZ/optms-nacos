package com.yexx.core.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Description: Long序列化, 防止Long的精度丢失
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/11/02 14:43
 */
@SuppressWarnings("ALL")
public class JsonLongSerializer extends JsonSerializer<Long> {
    private static final Long MAX_VALUE = 1000_0000_0000_0000L;

    public JsonLongSerializer() {
    }

    public void serialize(Long o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (o == null) {
            jsonGenerator.writeNull();
        }

        if (o > MAX_VALUE) {
            jsonGenerator.writeString(o.toString());
        } else {
            jsonGenerator.writeNumber(o);
        }

    }
}