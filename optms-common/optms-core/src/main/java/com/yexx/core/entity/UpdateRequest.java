package com.yexx.core.entity;

import com.yexx.core.entity.dto.Command;

/**
 * @author: Guosheng.Zhang
 * @date: 2020/8/10
 */
public abstract class UpdateRequest extends Command {

    /**
     * 返回主键
     *
     * @return 主键
     */
    public abstract Long getId();

}
