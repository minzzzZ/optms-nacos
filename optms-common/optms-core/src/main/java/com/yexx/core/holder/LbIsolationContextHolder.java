package com.yexx.core.holder;

import com.alibaba.ttl.TransmittableThreadLocal;

/**
 * Description: 负载均衡隔离Holder
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/07-16:48
 */
public class LbIsolationContextHolder {

    private static final ThreadLocal<String> VERSION_CONTEXT = new TransmittableThreadLocal<>();

    public static void setVersion(String version) {
        VERSION_CONTEXT.set(version);
    }

    public static String getVersion() {
        return VERSION_CONTEXT.get();
    }

    public static void clear() {
        VERSION_CONTEXT.remove();
    }
}
