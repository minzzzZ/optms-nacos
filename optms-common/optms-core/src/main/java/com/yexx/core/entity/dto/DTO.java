package com.yexx.core.entity.dto;

import java.io.Serializable;

/**
 * Data Transfer object, including Command, Query and Response,
 *
 * Command and Query is CQRS concept.
 *
 * Command Query Responsibility Segregation : 命令查询的责任分离.
 *
 * Command : 命令(新增, 更新, 删除)
 *
 * Query : 查询(顾名思义)
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/18-16:50
 */
public abstract class DTO implements Serializable {

}
