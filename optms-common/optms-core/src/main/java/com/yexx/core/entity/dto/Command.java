package com.yexx.core.entity.dto;

/**
 * Command request from Client.
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/18-17:02
 */
public abstract class Command extends DTO {

}
