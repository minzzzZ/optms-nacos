package com.yexx.core.entity.dto;

/**
 * Query request from Client.
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/18-17:03
 */
public abstract class Query extends DTO {

}
