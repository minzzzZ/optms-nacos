package com.yexx.core.entity;

import com.yexx.core.entity.dto.Command;

/**
 * delete command for caller
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/18-17:35
 */
public abstract class DeleteRequest extends Command {

    /** 主键id */
    private Long id;
}
