package com.yexx.core.entity;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yexx.core.entity.dto.Query;

/**
 * @author: Guosheng.Zhang
 * @date: 2020/8/10
 */
public abstract class LimitRequest extends Query {

    private Integer offset = 0;
    private Integer limit = 500;

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     * 转换成查询参数
     *
     * @param <T> 查询参数格式对象
     * @return 查询参数
     */
    public <T> QueryWrapper<T> toQueryWapper() {
        QueryWrapper<T> queryWrapper = toParamsQueryWapper();
        queryWrapper.last("limit " + getOffset() + "," + getLimit());
        return queryWrapper;
    }

    public abstract <T> QueryWrapper<T> toParamsQueryWapper();

}
