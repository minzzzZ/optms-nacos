package com.yexx.core.constant;

/**
 * Description: 业务码
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/18-17:25
 */
public final class BizCode {

    /** 正常码 */
    public static final Integer OK = 10000;

    /** 异常码 */
    public static final Integer EXCEPTION = 20000;

    /** 错误码 */
    public static final Integer ERROR = 50000;

}
