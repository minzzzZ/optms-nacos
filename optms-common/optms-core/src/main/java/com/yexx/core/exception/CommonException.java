package com.yexx.core.exception;

public class CommonException extends RuntimeException {
    private Integer code;
    private String message;
    private Object data;

    public CommonException(Integer code, String message) {
        super(message);
        this.message = message;
        this.code = code;
    }

    public CommonException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.message = message;
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}