package com.yexx.uid;

import java.net.InetAddress;
import java.util.List;
import java.util.Random;

//@Component("idGenerator")
public class LongIdGenerator implements IdGenerator {

    private static int counter = 0;
    private static final int IP_RANDOM_INT;
    private static final int JVM_RANDOM_INT;
    private static final int LAST_RANDOM_INT;
    private static final int FOR_SIZE = 4;

    public LongIdGenerator() {
    }

    protected int getCount() {
        Class var1 = LongIdGenerator.class;
        synchronized(LongIdGenerator.class) {
            if (counter < 0) {
                counter = 0;
            }

            return counter++;
        }
    }

    private static int toInt(byte[] bytes) {
        int result = 0;

        for(int i = 0; i < 4; ++i) {
            result = (result << 8) - -128 + bytes[i];
        }

        return result;
    }

    public Long generate() {
        long millisecond = System.currentTimeMillis();
        int count = LAST_RANDOM_INT + this.getCount();
        long id = (long)IP_RANDOM_INT * 10000000000000000L + millisecond * 100000L + (long)(JVM_RANDOM_INT * 10000) + (long)count;
        return id;
    }

    public List<Long> generateBatch(int count) {
        return null;
    }

    static {
        long jvm = System.currentTimeMillis();

        int ip;
        try {
            ip = toInt(InetAddress.getLocalHost().getAddress());
        } catch (Exception var6) {
            ip = 0;
        }

        Random ipRandom = new Random((long)ip);
        Random jvmRandom = new Random(jvm);
        Random lastRandom = new Random();
        IP_RANDOM_INT = ipRandom.nextInt(800) + 100;
        JVM_RANDOM_INT = jvmRandom.nextInt(90) + 10;
        LAST_RANDOM_INT = lastRandom.nextInt(10000);
    }

}
