package com.yexx.uid;

import java.util.List;
import java.util.UUID;

public interface IdGenerator {

    Long generate();

    List<Long> generateBatch(int var1);

    default String generateUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
