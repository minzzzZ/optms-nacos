package com.yexx.gateway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.util.StopWatch;

/**
 * Description: 启动
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/16-13:59
 */
@Slf4j
@ConfigurationPropertiesScan("com.yexx")
@SpringBootApplication(scanBasePackages = "com.yexx")
public class GatewayApplication {

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("run-application");
        SpringApplication.run(GatewayApplication.class, args);
        stopWatch.stop();
        log.info("run end！ {}", stopWatch.prettyPrint());
    }

}
