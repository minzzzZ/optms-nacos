package com.yexx.service.search;

import com.yexx.service.search.entity.TestEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Description: 测试类
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/11/16-11:54
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchApplication.class)
public class SearchTestController {


    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Test
    public void test() {
        boolean test_id = elasticsearchRestTemplate.exists("1", TestEntity.class);
        System.out.println(test_id);
    }

    @Test
    public void testCreateIndex(){
        boolean index = elasticsearchRestTemplate.createIndex(TestEntity.class);
    }

    @Test
    public void testSearch(){
    }
}
