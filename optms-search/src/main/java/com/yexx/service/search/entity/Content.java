package com.yexx.service.search.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: es-api
 * @description:
 * @author: FangZhen
 * @create: 2020-11-18 16:46
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Content {

    private String title;
    private String price;
    private String img;
}
