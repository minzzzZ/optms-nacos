package com.yexx.service.search.util;

import com.yexx.service.search.entity.Content;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: es-api
 * @description:
 * @author: FangZhen
 * @create: 2020-11-18 16:20
 **/
public class HtmlParseUtil {

    public static void main(String[] args) throws IOException {
        new HtmlParseUtil().parseJD("java").forEach(System.out::println);
    }

    public List<Content> parseJD(String keywords) throws IOException {
        //获取请求  https://search.jd.com/search?keyword=java
        // 前提 需要联网 ajax
        String url= "https://search.jd.com/Search?keyword=" + keywords;
        //解析网页 (jsoup 返回document就是浏览器document对象)
        Document document = Jsoup.parse(new URL(url), 30000);
        //所有你在js中可以使用的方法，在这里都能使用
        Element element = document.getElementById("J_goodsList");
//        System.out.println(element.html());
        //获取所有的li元素
        Elements elements = element.getElementsByTag("li");
        List<Content> goodsList = new ArrayList<>();
        //获取元素中的内容，这里el就是每一个li标签了
        for (Element el : elements) {
            //关于这种图片特别多的网站 都是延迟加载的
            String img = el.getElementsByTag("img").eq(0).attr("data-lazy-img");
            String price = el.getElementsByClass("p-price").eq(0).text();
            String title = el.getElementsByClass("p-name").eq(0).text();
            goodsList.add(new Content(title, price, img));
        }
        return goodsList;
    }
}
