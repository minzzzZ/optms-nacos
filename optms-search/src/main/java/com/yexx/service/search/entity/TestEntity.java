package com.yexx.service.search.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * Description: 测试
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/11/16-13:10
 */
@Data
@Document(indexName = "test_entity", type = "_doc")
public class TestEntity {

    @Id
    private Long id;

    private String content;
}
